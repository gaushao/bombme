module.exports = (PLAYER_LIST) => {
    let ranking = [];
    for(id in PLAYER_LIST){
        ranking.push({name:PLAYER_LIST[id].name,frag:PLAYER_LIST[id].frag});
    }
    ranking.sort(function compare(a, b) {
        if (a.frag > b.frag) return -1;
        if (a.frag < b.frag) return 1;
        return 0;
    });
    //io.sockets.emit('ranking', ranking);
    return ranking;
};