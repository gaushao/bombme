const fs = require('fs');

class Log{
    constructor(){
        this.dir = './logs';
        this.isLoggin = true;
        if(this.isLoggin) this.newFile();
    }
    newFile(){
        this.date = new Date();
        this.data = { 
            date : this.date.getFullYear().toString() + '-' + (this.date.getMonth()+1).toString() + '-' + this.date.getDate().toString() + '-' + (this.date.getHours()+1).toString() + '-' + (this.date.getMinutes()+1).toString() + '-' + (this.date.getSeconds()+1).toString(),
            deads : []
        };
        this.dir = this.getDir();
        this.path = this.getPath();
    }
    getDir(){
        let dir = this.dir;
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
        return dir;
    }
    getPath(){
        let path = this.dir + '/' + this.data.date + '.json';
        fs.writeFileSync(path, JSON.stringify(this.data), function(err){
            if(err){console.log(err);}
        });
        return path;
    }
    dead(player){
        if(this.isLoggin){
            let log = {
                position : player.position,
                isAI : player.isAI,
                frags : player.frag,
                experience : player.exp,
                abilities : player.abilities,
                strategy: player.strategy
            };
            if(this.data.deads.length >= 500){this.newFile();}
            this.data.deads.push(log);
            fs.writeFile(this.path, JSON.stringify(this.data), 'utf8', function(err){
                if(err){console.log(err);}
            })
        }
    }
}

module.exports = new Log();