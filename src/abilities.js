const fs = require('fs');
module.exports = class Abilities{
    constructor(){
        let config = this.loadConfig('./abilities.config');
        this.movespeed = config.movespeed;
        this.cooldown = config.cooldown;
        this.bombRange = config.bombRange;
        this.bombDelay = config.bombDelay;
    }
    loadConfig(path){
        let config = {};
        //  LOAD CONFIG FILE IF EXISTS
        if(fs.existsSync(path)){
            config = JSON.parse(fs.readFileSync(path, 'utf8'));
            Object.keys(config).forEach(function (key) {
                config[key] = this.parseObj(config[key]);
            }.bind(this));
        }
        //  OR CREATE A DEFAULT CONFIG FILE
        else{
            config = {
                movespeed : this.parseObj(
                    {
                        base : 600,
                        maxLv : 50,
                        level : 0,
                        getCost : function(){
                            this.cost = (this.level + 1) * 10;
                            return this.cost;
                        },
                        getValue : function(){
                            this.value = this.base - (this.level * 10);
                            return this.value;
                        },
                        upgrade : function(player){
                            player.movingDelay = this.getValue();
                            return player.movingDelay;
                        },
                    }
                ),
                cooldown : this.parseObj(
                    {
                        base : 4500,
                        maxLv : 40,
                        level : 0,
                        getCost : function(){
                            this.cost = (this.level + 1) * 10;
                            return this.cost;
                        },
                        getValue : function(){
                            this.value = this.base - (this.level * 100);
                            return this.value;
                        },
                        upgrade : function(player){
                            player.coolingDelay = this.getValue();
                            return player.coolingDelay;
                        },
                    }
                ),
                bombRange : this.parseObj(
                    {
                        base : 1,
                        maxLv : 50,
                        level : 0,
                        getCost : function(){
                            this.cost = (this.level + 1) * 25;
                            return this.cost;
                        },
                        getValue : function(){
                            this.value = this.base + this.level;
                            return this.value;
                        },
                        upgrade : function(player){
                            player.bombRange = this.getValue();
                            return player.bombRange;
                        },
                    }
                ),
                bombDelay : this.parseObj(
                    {
                        base : 4500,
                        maxLv : 40,
                        level : 0,
                        getCost : function(){
                            this.cost = (this.level + 1) * 5;
                            return this.cost;
                        },
                        getValue : function(){
                            this.value = this.base - (this.level * 100);
                            return this.value;
                        },
                        upgrade : function(player){
                            player.bombDelay = this.getValue();
                            return player.bombDelay;
                        },
                    }
                ),
            };
            fs.writeFileSync(path,JSON.stringify(config), 
            function(err){
                if(err){console.log(err);}
                else{}
            });
            config = this.loadConfig(path);
        }
        config.movespeed.getCost();
        config.movespeed.getValue();
        config.cooldown.getCost();
        config.cooldown.getValue();
        config.bombRange.getCost();
        config.bombRange.getValue();
        config.bombDelay.getCost();
        config.bombDelay.getValue();
        return config;
    }
    parseObj(obj){
        obj = Object.assign({}, obj);
        Object.keys(obj).forEach(function (key) {
            switch(typeof obj[key]){
                case 'string':
                obj[key] = eval('(' + obj[key] + ')');
                break;
                case 'function':
                obj[key] = obj[key] + '';
                break;
                default:
                obj[key] = obj[key];
                break;
            }
        });
        return obj;
    }
    upgrade(player, abName, io, socket){
        let done = false;
        if(['movespeed','cooldown','bombRange','bombDelay'].includes(abName)){
            let ability = eval("this." + abName);
            if(player.exp >= ability.getCost() && ability.level <= ability.maxLv){
                player.exp -= ability.getCost();
                ability.level++;
                let data = {
                    playerId : player.id,
                    name : abName,
                    level : ability.level,
                    cost : ability.getCost(),
                    value : ability.upgrade(player)
                }
                io.sockets.emit('playerUpgrades', data);
                done = true;
            }
            if(!player.isAI){io.sockets.sockets[socket].emit('exp', player.exp);}
        }
        return done;
    }
}