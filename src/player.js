const LOG = require('./log');
const Abilities = require('./abilities');
const Bomb = require('./bomb');
module.exports = class Player{
    constructor(id, name, map){
        this.id = id;
        this.isFake = false;
        this.isAI = false;
        this.strategy = 'notAI';
        this.name = name;
        this.frag = 0;
        this.exp = 0;
        this.alive = true;
        this.moving = false;
        this.cooling = false;
        this.position = {
            x : Math.floor((map.tiles.length-1)/2),
            y : Math.floor((map.tiles.length-1)/2)
        };
        if(map.tiles[this.position.x][this.position.y] == 1){
            let rand = Math.floor(Math.random()*8);
            switch(rand){
                case 0:
                this.position.x++;
                break;
                case 1:
                this.position.x--;
                break;
                case 2:
                this.position.y++;
                break;
                case 3:
                this.position.y--;
                break;
                case 4:
                this.position.x++;
                this.position.y++;
                break;
                case 5:
                this.position.x--;
                this.position.y++;
                break;
                case 6:
                this.position.x++;
                this.position.y--;
                break;
                case 7:
                this.position.x--;
                this.position.y--;
                break;
            }
        }
        this.abilities = new Abilities();
        this.movingDelay = this.abilities.movespeed.getValue();
        this.coolingDelay = this.abilities.cooldown.getValue();
        this.bombDelay = this.abilities.bombDelay.getValue();
        this.bombRange = this.abilities.bombRange.getValue();
    }
    move(direction, io, map){
        let result = false;
        if(this.alive && !this.moving){
            switch(direction){
                case 'up':
                if(map.checkTile(this.position.x,this.position.y-1) == 0){
                    this.position.y -=1;
                    result = true;
                }
                break;
                case 'down':
                if(map.checkTile(this.position.x,this.position.y+1) == 0){
                    this.position.y +=1;
                    result = true;
                }
                break;
                case 'left':
                if(map.checkTile(this.position.x-1,this.position.y) == 0){
                    this.position.x -=1;
                    result = true;
                }
                break;
                case 'right':
                if(map.checkTile(this.position.x+1,this.position.y) == 0){
                    this.position.x +=1;
                    result = true;
                }
                break;
            }
        }
        if(result){
            this.moving = true;
            let self = this;
            setTimeout(function(){
                self.moving = false;
                io.sockets.emit('playerStands', self.id);
            }, self.movingDelay);
            io.sockets.emit('playerChanges', this);
            return true;
        }else{return false;}
    }
    bomb(io, socket, BOMB_LIST, bombId, MAP, PLAYER_LIST, SOCKET_LIST){
        if(!this.cooling){
            BOMB_LIST[bombId] = new Bomb(bombId, this, io, MAP, PLAYER_LIST, BOMB_LIST, SOCKET_LIST);
            bombId++;
            this.cooling = true;
            let self = this;
            setTimeout(function(){
                if(self.alive){
                    self.cooling = false;
                    if(!self.isAI){io.sockets.sockets[socket].emit('playerCools', self.cooling);}
                }
            },self.coolingDelay);
        }
        return bombId;
    }
    die(byName, io, PLAYER_LIST){
        if(!this.isFake){LOG.dead(this);}
        this.alive = false;
        io.sockets.emit('playerDies', {dead: this.id, by: byName});
        //if (this.name && byName) console.log(`${this.name} was killed by ${byName}`)
        delete PLAYER_LIST[this.id];
        delete this;
    }
}