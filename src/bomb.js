const getRanking = require('./ranking');
const { findValue } = require('./util');
module.exports = class Bomb{
    constructor(bombId, player, io, MAP, PLAYER_LIST, BOMB_LIST, SOCKET_LIST){
        this.id = bombId;
        this.player = player;
        this.playerId = player.id;
        this.playerName = player.name;
        this.position = {x : this.player.position.x, y : this.player.position.y};
        this.alive = true;
        this.delay = player.bombDelay;
        this.range = player.bombRange;
        io.sockets.emit('bombDrop', this);
        let self = this;
        setTimeout(function(){
            self.explode(io, MAP, PLAYER_LIST, BOMB_LIST, SOCKET_LIST);
        }, self.delay);
    }
    explode(io, MAP, PLAYER_LIST, BOMB_LIST, SOCKET_LIST){
        let hit = [];
        let explos = [];
        let x = this.position.x;
        let y = this.position.y;
        io.sockets.emit('bombExplodes', this.id);
        explos.push({x: x, y: y});
        for(let d = 0; d < 4; d++){
            let x = this.position.x;
            let y = this.position.y;
            for(let r = 1; r <= this.range; r++){
                if((d == 0 && MAP.checkTile(x+1,y) == 0
                || d == 1 && MAP.checkTile(x-1,y)== 0
                || d == 2 && MAP.checkTile(x,y+1) == 0
                || d == 3 && MAP.checkTile(x,y-1) == 0)){
                    switch(d){
                        case 0:
                            x += 1;
                        break;
                        case 1:
                            x -= 1;
                        break;
                        case 2:
                            y += 1;
                        break;
                        case 3:
                            y -= 1;
                        break;
                    }
                    explos.push({x: x, y: y});
                }else{break;}
            }
        }
        io.sockets.emit('explosion', explos);
        for(let id in PLAYER_LIST){
            for(let explo of explos){
                if(PLAYER_LIST[id].alive && PLAYER_LIST[id].position.x == explo.x && PLAYER_LIST[id].position.y == explo.y){
                    hit.push(id);
                }
            }
        }
        if(hit.length){
            for(let id of hit){
                if(PLAYER_LIST[this.playerId] && this.playerName != undefined){
                    PLAYER_LIST[this.playerId].frag++;
                    PLAYER_LIST[this.playerId].exp += 10;
                    if(!this.player.isAI){io.sockets.sockets[findValue(SOCKET_LIST, this.playerId)].emit('exp', PLAYER_LIST[this.playerId].exp)};
                }
                PLAYER_LIST[id].die(this.playerName, io, PLAYER_LIST);
                io.sockets.emit('ranking', getRanking(PLAYER_LIST));
            }
        }
        delete BOMB_LIST[this.id];
        delete this;
    }
}