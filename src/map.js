const Bomb = require('./bomb');
const Player = require('./player')
module.exports = class Map{
    constructor(sizeBase){
        this.sizeBase = sizeBase;
        this.size = this.sizeBase;
        this.willChange = false;
        this.tiles = this.newTiles();
    }

    resize(io, PLAYER_LIST, BOMB_LIST, bombId, MAP, SOCKET_LIST){
        if(!this.willChange){
            let level = (this.size-this.sizeBase)/2;
            let playerAmmount = Object.keys(PLAYER_LIST).length;
            let rate = 4-level*3;
            if(this.size <= playerAmmount + rate){
                this.size += 2;
                console.log("MAP SIZE INCREASES => Size:", this.size, ", Player Ammount:", playerAmmount);
            }else if (playerAmmount && (this.size > 5 + playerAmmount + rate)){
                this.willChange = true;
                let player = new Player(-1, '', this);
                PLAYER_LIST[this.id] = player;
                player.isAI = true;
                player.isFake = true;
                player.bombRange = 1;
                for(let x = 1; x < this.tiles.length-2; x++){
                    player.position.x = x;
                    player.position.y = 1;
                    BOMB_LIST[bombId] = new Bomb(bombId, player, io, MAP, PLAYER_LIST, BOMB_LIST, SOCKET_LIST);
                    bombId++;
                    player.position.y = this.tiles.length-2;
                    BOMB_LIST[bombId] = new Bomb(bombId, player, io, MAP, PLAYER_LIST, BOMB_LIST, SOCKET_LIST);
                    bombId++;
                }
                for(let y = 1; y < this.tiles.length-2; y++){
                    player.position.x = 1;
                    player.position.y = y;
                    BOMB_LIST[bombId] = new Bomb(bombId, player, io, MAP, PLAYER_LIST, BOMB_LIST, SOCKET_LIST);
                    bombId++;
                    player.position.x = this.tiles.length-2;
                    BOMB_LIST[bombId] = new Bomb(bombId, player, io, MAP, PLAYER_LIST, BOMB_LIST, SOCKET_LIST);
                    bombId++;
                }
                player.position.x = this.tiles.length-2;
                player.position.y = this.tiles.length-2;
                BOMB_LIST[bombId] = new Bomb(bombId, player, io, MAP, PLAYER_LIST, BOMB_LIST, SOCKET_LIST);
                bombId++;
                let self = this;
                setTimeout(function(){
                    self.willChange = false;
                    self.size -= 2;
                    console.log("MAP SIZE DECREASES => Size:", self.size, ", Player Ammount:", playerAmmount);
                    self.update(io);
                    self.resize(io, PLAYER_LIST, BOMB_LIST, bombId, MAP, SOCKET_LIST);
                }, 4500);
            }
        }else{
            console.log("MAP SIZE => WILL DECREASE");
        }
        this.update(io);
        return bombId;
    }

    newTiles(){
        let size = this.size;
        let tiles = [];
        for(let x = 0; x < size; x++) {
            tiles.push([]);
            for(let y = 0; y < size; y++) {
                if(x == 0 || y == 0 || x == size - 1 || y == size - 1){
                    tiles[x][y] = 1;
                }else if(x == 1 || y == 1 || x == size-2 || y == size - 2){
                    tiles[x][y] = 0;
                }else if (x % 2 == 0 && y % 2 == 0){
                    tiles[x][y] = 1;
                }else{
                    tiles[x][y] = 0;
                }
            }
        }
        return tiles;
    }

    update(io){
        this.tiles = this.newTiles();
        return io.sockets.emit('map', this.tiles);
    }

    checkTile(x,y){
        if(typeof this.tiles[x] === 'undefined' || typeof this.tiles[x][y] === 'undefined'){
            return null;
        }else{return this.tiles[x][y];}
    }
}
