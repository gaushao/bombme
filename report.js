const express = require('express');
const app = express();
const serv = require('http').Server(app);
const fs = require('fs');
const io = require('socket.io')(serv,{});
const logsDir = './logs';

function getFiles(dir, files_){
    files_ = files_ || [];
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
    var files = fs.readdirSync(dir);
    for (var i in files){
        var name = dir + '/' + files[i];
        if (fs.statSync(name).isDirectory()){
            getFiles(name, files_);
        } else {
            files_.push(name);
        }
    }
    return files_;
}

function getFile(path){
    if(path.includes(".json") && (path.length - path.indexOf(".json")) == 5){
        if(fs.existsSync(path)){
            let file = JSON.parse(fs.readFileSync(path, 'utf8'));
            return file;
        }
    }
}
function validateNumber(n){
    return isFinite(n || 0) ? n || 0 : 0.0;
}
function report(log){
    if(log == undefined || log.deads.length == 0){return false;}
    
    let report = {
        players : log.deads.length,
        totalFrags : 0,
        totalExp : 0,
        totalUpgrades : 0,
        humans : 0,
        robots : 0,
        playersFragsRate : 0,
        humansFragsRate : 0,
        robotsFragsRate : 0,
        abilities : {
            movespeed : {
                totalLevel : 0,
                cost : log.deads[0].abilities.movespeed.cost,
                strategy : {}
            },
            cooldown : {
                totalLevel : 0,
                cost : log.deads[0].abilities.cooldown.cost,
                strategy : {}
            },
            bombRange : {
                totalLevel : 0,
                cost : log.deads[0].abilities.bombRange.cost,
                strategy : {}
            },
            bombDelay : {
                totalLevel : 0,
                cost : log.deads[0].abilities.bombDelay.cost,
                strategy : {}
            }
        }
    };

    let strategy = {
        chosenBy : 0,
        totalExp : 0,
        totalLevel : 0,
        totalFrags : 0,
        fragsLevelRate : 0
    };

    report.abilities.movespeed.strategy.robots = Object.assign({}, strategy);
    report.abilities.movespeed.strategy.humans = Object.assign({}, strategy);
    report.abilities.cooldown.strategy.robots = Object.assign({}, strategy);
    report.abilities.cooldown.strategy.humans = Object.assign({}, strategy);
    report.abilities.bombRange.strategy.robots = Object.assign({}, strategy);
    report.abilities.bombRange.strategy.humans = Object.assign({}, strategy);
    report.abilities.bombDelay.strategy.robots = Object.assign({}, strategy);
    report.abilities.bombDelay.strategy.humans = Object.assign({}, strategy);

    for(let dead in log.deads){
        if(log.deads[dead].isAI){report.robots++;}
        else{report.humans++;}
        switch(log.deads[dead].strategy){
            case "movespeed":
            report.abilities.movespeed.strategy.robots.chosenBy++;
            report.abilities.movespeed.strategy.robots.totalExp += log.deads[dead].experience;
            report.abilities.movespeed.strategy.robots.totalLevel += log.deads[dead].abilities.movespeed.level;
            report.abilities.movespeed.strategy.robots.totalFrags += log.deads[dead].frags;
            break;
            case "cooldown":
            report.abilities.cooldown.strategy.robots.chosenBy++;
            report.abilities.cooldown.strategy.robots.totalExp += log.deads[dead].experience;
            report.abilities.cooldown.strategy.robots.totalLevel += log.deads[dead].abilities.cooldown.level;
            report.abilities.cooldown.strategy.robots.totalFrags += log.deads[dead].frags;
            break;
            case "bombRange":
            report.abilities.bombRange.strategy.robots.chosenBy++;
            report.abilities.bombRange.strategy.robots.totalExp += log.deads[dead].experience;
            report.abilities.bombRange.strategy.robots.totalLevel += log.deads[dead].abilities.bombRange.level;
            report.abilities.bombRange.strategy.robots.totalFrags += log.deads[dead].frags;
            break;
            case "bombDelay":
            report.abilities.bombDelay.strategy.robots.chosenBy++;
            report.abilities.bombDelay.strategy.robots.totalExp += log.deads[dead].experience;
            report.abilities.bombDelay.strategy.robots.totalLevel += log.deads[dead].abilities.bombDelay.level;
            report.abilities.bombDelay.strategy.robots.totalFrags += log.deads[dead].frags;
            break;
            case "notAI":
            if(log.deads[dead].abilities.movespeed.level > 0){
                report.abilities.movespeed.strategy.humans.chosenBy++;
                report.abilities.movespeed.strategy.humans.totalExp += log.deads[dead].experience;
                report.abilities.movespeed.strategy.humans.totalLevel += log.deads[dead].abilities.movespeed.level;
                report.abilities.movespeed.strategy.humans.totalFrags += log.deads[dead].frags;
            }
            if(log.deads[dead].abilities.cooldown.level > 0){
                report.abilities.cooldown.strategy.humans.chosenBy++;
                report.abilities.cooldown.strategy.humans.totalExp += log.deads[dead].experience;
                report.abilities.cooldown.strategy.humans.totalLevel += log.deads[dead].abilities.cooldown.level;
                report.abilities.cooldown.strategy.humans.totalFrags += log.deads[dead].frags;
            }
            if(log.deads[dead].abilities.bombRange.level > 0){
                report.abilities.bombRange.strategy.humans.chosenBy++;
                report.abilities.bombRange.strategy.humans.totalExp += log.deads[dead].experience;
                report.abilities.bombRange.strategy.humans.totalLevel += log.deads[dead].abilities.bombRange.level;
                report.abilities.bombRange.strategy.humans.totalFrags += log.deads[dead].frags;
            }
            if(log.deads[dead].abilities.bombDelay.level > 0){
                report.abilities.bombDelay.strategy.humans.chosenBy++;
                report.abilities.bombDelay.strategy.humans.totalExp += log.deads[dead].experience;
                report.abilities.bombDelay.strategy.humans.totalLevel += log.deads[dead].abilities.bombDelay.level;
                report.abilities.bombDelay.strategy.humans.totalFrags += log.deads[dead].frags;
            }
            break;
            default:
            break;
        }
        report.totalExp += log.deads[dead].experience;
        report.totalFrags += log.deads[dead].frags;
        report.abilities.movespeed.totalLevel += log.deads[dead].abilities.movespeed.level;
        report.abilities.cooldown.totalLevel += log.deads[dead].abilities.cooldown.level;
        report.abilities.bombRange.totalLevel += log.deads[dead].abilities.bombRange.level;
        report.abilities.bombDelay.totalLevel += log.deads[dead].abilities.bombDelay.level;
    }
    report.playersFragsRate = validateNumber(report.totalFrags / report.players);
    report.robotsFragsRate = validateNumber(report.totalFrags / report.robots);
    report.humansFragsRate = validateNumber(report.totalFrags / report.humans);

    report.abilities.movespeed.strategy.robots.fragsLevelRate = validateNumber(report.abilities.movespeed.strategy.robots.totalFrags / report.abilities.movespeed.strategy.robots.totalLevel);
    report.abilities.cooldown.strategy.robots.fragsLevelRate = validateNumber(report.abilities.cooldown.strategy.robots.totalFrags / report.abilities.cooldown.strategy.robots.totalLevel);
    report.abilities.bombRange.strategy.robots.fragsLevelRate = validateNumber(report.abilities.bombRange.strategy.robots.totalFrags / report.abilities.bombRange.strategy.robots.totalLevel);
    report.abilities.bombDelay.strategy.robots.fragsLevelRate = validateNumber(report.abilities.bombDelay.strategy.robots.totalFrags / report.abilities.bombDelay.strategy.robots.totalLevel);

    report.abilities.movespeed.strategy.humans.fragsLevelRate = validateNumber(report.abilities.movespeed.strategy.humans.totalFrags / report.abilities.movespeed.strategy.humans.totalLevel);
    report.abilities.cooldown.strategy.humans.fragsLevelRate = validateNumber(report.abilities.cooldown.strategy.humans.totalFrags / report.abilities.cooldown.strategy.humans.totalLevel);
    report.abilities.bombRange.strategy.humans.fragsLevelRate = validateNumber(report.abilities.bombRange.strategy.humans.totalFrags / report.abilities.bombRange.strategy.humans.totalLevel);
    report.abilities.bombDelay.strategy.humans.fragsLevelRate = validateNumber(report.abilities.bombDelay.strategy.humans.totalFrags / report.abilities.bombDelay.strategy.humans.totalLevel);

    log.report = report;
    return log;
}

//START A SERVER BY EXPRESS
app.get('/', function(req, res){
    res.sendFile(__dirname + '/client/showlogs.html');
});
app.use('/client', express.static(__dirname + '/client'));
serv.listen(2051, function() {
    console.log('Server ready on port %d', serv.address().port);
});

//MANAGE SOCKETS
const SOCKET_LIST = {};
var userID = 0;
io.sockets.on('connection', function(socket){
    console.log('New client: ', userID, socket.id);
    SOCKET_LIST[socket.id] = userID;
    userID++;

    socket.on('showLogsDir', function(){
        socket.emit('showLogsDir', getFiles(logsDir));
    });
    socket.on('showLog', function(data){
        socket.emit('showLog', report(getFile(data)));
    });
    socket.on('disconnect', function(){
        delete SOCKET_LIST[socket.id];
        console.log('Client left: ', socket.id);
    });
});