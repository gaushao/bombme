/**BUGS
 * cancelar nick no mobile
 * client inicia com o tap da ultima partida
 * jaera ? > erro nos icones de upgrade qnd bot ganha exp
 * nao renderiza as bombas dropadas antes do login
 */

var socket = io();
var config = {
	type:Phaser.AUTO,
	width:800,
	height:600,
	scene:{
        preload: preload,
		create: create,
		update: update
    }
};

var game = new Phaser.Game(config);
var stance = 'INTRO';
var tempInterface = {drawn : false};
var tiles, client = {};
const TILESIZE = 32;
var map = [];
var playerList = {};
var bombList = {};
var past = {map : [], playerList : {}, bombList : {}};

var thisDevice = {
    Android: function() {
        return /Android/i.test(navigator.userAgent);
    },
    BlackBerry: function() {
        return /BlackBerry/i.test(navigator.userAgent);
    },
    iOS: function() {
        return /iPhone|iPad|iPod/i.test(navigator.userAgent);
    },
    Opera: function() {
        return /Opera Mini/i.test(navigator.userAgent);
    },
    Windows: function() {
        return /IEMobile/i.test(navigator.userAgent);
    },
    isMobile: function() {
        return (thisDevice.Android() || thisDevice.BlackBerry() || thisDevice.iOS() || thisDevice.Opera() || thisDevice.Windows());
    }
};

function listenSocket(game){
    socket.on('login', function(data){
        if(data){
            stance = 'RUN';
        }else{
            stance = 'INTRO';
        }
    });
    socket.on('clientId', function(data){
        client.id = data;
    });
    socket.on('map', function(data){
        map = data;
    });
    socket.on('playerList', function(data){
        playerList = data;
    });
    socket.on('playerChanges', function(data){
        if(stance=='RUN'){playerList[data.id] = data;}
    });
    socket.on('playerStands', function(data){
        if(stance=='RUN'){if(past.playerList[data])past.playerList[data].moving = false;}
    });
    socket.on('playerCools', function(data){
        if(stance=='RUN'){client.player.cooling = data;}
    });
    socket.on('playerDies', function(data){
        if(stance=='RUN'){
            if(playerList[data.dead]){playerList[data.dead].alive = false;}
            if(data.by != undefined){client.murderer = data.by;}else{client.murderer = 'explosion'}
        }
    });
    socket.on('playerUpgrades', function(data){
        if(stance=='RUN'){
            past.playerList[data.playerId].upgrade(data);
        }
    });
    socket.on('bombList', function(data){
        if(stance=='RUN'){bombList = data;}
    });
    socket.on('bombDrop', function(data){
        if(stance=='RUN'){bombList[data.id] = data;}
    });
    socket.on('bombExplodes', function(data){
        if(stance=='RUN'){
            if(bombList[data]){bombList[data].alive = false;}
        }
    });
    socket.on('explosion', function(data){
        if(stance=='RUN'){
            for(let explo of data){new Explosion(game, explo.x, explo.y);}
        }
    });
    socket.on('ranking', function(data){
        ranking.data = data;
    });
    socket.on('exp', function(data){
        //console.log(data);
        client.player.exp = data;
    });
};

var cursorKey = game.input.keyboard.createCursorKeys();
var spaceKey = game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
var pointer = game.input.activePointer;

function listenKeys(game){
    if(client.player && stance == 'RUN'){
        if(pointer.isDown){
            let px = (pointer.position.x*100)/800;
            let py = (pointer.position.y*100)/600;
            let mdx = Math.abs(px - 50);
            let mdy = Math.abs(py - 50);
            if((mdx < 5) && (mdy < 5) && !client.player.cooling){
                socket.emit('key', 'space');
            }
            else if(!client.player.moving){
                if(px > py){
                    if(100-px < py){socket.emit('key', 'right');}
                    else{socket.emit('key', 'up');}
                }
                else{
                    if(px < 100-py){socket.emit('key', 'left');}
                    else{socket.emit('key', 'down');}
                }
            }
        }else{
            if(spaceKey.isDown && !client.player.cooling){
                socket.emit('key', 'space');
            }
            if(!client.player.moving){
                if(cursorKey.up.isDown){
                    socket.emit('key', 'up');
                }else if(cursorKey.down.isDown){
                    socket.emit('key', 'down');
                }else if(cursorKey.left.isDown){
                    socket.emit('key', 'left');
                }else if(cursorKey.right.isDown){
                    socket.emit('key', 'right');
                }
            }
        }
    }
};

function resizeWindow(){
    game.renderer.resize(window.innerWidth, window.innerHeight, 1.0);
    game.scene.scenes[0].cameras.main.width = window.innerWidth;
    game.scene.scenes[0].cameras.main.height = window.innerHeight;
};

window.onresize = function(){
    resizeWindow();
};

function cameraFollow(game){
    game.scene.scene.cameras.main.x = Math.round((game.scene.scene.cameras.main.width/2) - client.player.body.x);
    game.scene.scene.cameras.main.y = Math.round((game.scene.scene.cameras.main.height/2) - client.player.body.y);
};

class inputTextbox{
    constructor(game){
        this.body = game.add.text(0, 0);
        this.text = '';
    };
    listen(game, self){
        game.input.keyboard.on('keydown', function (event){
            switch (event.code){
                case 'Enter':
                    game.input.keyboard.removeAllListeners();
                    tempInterface.graphics.destroy();
                    tempInterface.text.destroy();
                    tempInterface.inputTextbox.body.destroy();
                    delete tempInterface.graphics;
                    delete tempInterface.background
                    delete tempInterface.text;
                    delete tempInterface.inputTextbox.body;
                    delete tempInterface.inputTextbox;
                    tempInterface = {drawn : false};
                    login(self.text);
                break;
                case 'Backspace':
                    self.backspace();
                break;
                default:
                if (self.text.length < 12)
                    if(event.keyCode >= 48 && event.keyCode <= 57
                    || event.keyCode >= 65 && event.keyCode <= 90
                    || event.keyCode >= 96 && event.keyCode <= 105
                    || event.keyCode == 144){
                        self.insert(event.key);
                    }
                break;
            }
        });
    };
    insert(char){
        this.text += char;
        this.body.setText(this.text, false);
    };
    backspace(){
		if (this.text.length > 0){this.text = this.text.slice(0, -1);}
        else{this.text = ''};
        this.body.setText(this.text, false);
    };
}

function login(playerName){
    socket.emit('login', playerName);
    stance = 'LOADING';
};

function gameIntro(game){
    if(!tempInterface.drawn){
        tempInterface.graphics = game.add.graphics({
            lineStyle: { width: 2, color: 0xaa0000 },
            fillStyle: { color: 0x0000aa }
        });
        tempInterface.background = new Phaser.Geom.Rectangle();
        tempInterface.text = game.add.text(0, 0, 'TYPE A NAME:');
        tempInterface.inputTextbox = new inputTextbox(game);
        tempInterface.inputTextbox.listen(game, tempInterface.inputTextbox);
        tempInterface.drawn = true;
    }else{
        tempInterface.graphics.clear();
        tempInterface.background.width = Math.round(game.scene.scene.cameras.main.width/2);
        tempInterface.background.height = Math.round(game.scene.scene.cameras.main.height/2.5);
        tempInterface.background.centerX = Math.round(game.scene.scene.cameras.main.width/2);
        tempInterface.background.centerY = Math.round(game.scene.scene.cameras.main.height/2);
        tempInterface.text.setFontSize(Math.round((tempInterface.background.width/20) + (tempInterface.background.height/15)));
        tempInterface.text.x = Math.round(tempInterface.background.centerX - (tempInterface.text.width/2));
        tempInterface.text.y = Math.round(tempInterface.background.centerY - (tempInterface.background.height/2.5));
        tempInterface.inputTextbox.body.x = Math.round(tempInterface.background.centerX - (tempInterface.inputTextbox.body.width/2));
        tempInterface.inputTextbox.body.y = tempInterface.text.y + (tempInterface.text.height*2);
        tempInterface.inputTextbox.body.setFontSize(tempInterface.text.height)
        tempInterface.graphics.fillRectShape(tempInterface.background);
        tempInterface.graphics.strokeRectShape(tempInterface.background);
    }
};

function restart(game){
    tempInterface.graphics.destroy();
    tempInterface.text.destroy();
    delete tempInterface.graphics;
    delete tempInterface.background
    delete tempInterface.text;
    tempInterface = {drawn : false};
    stance = 'INTRO';
    game.input.keyboard.removeAllListeners();
    game.input.removeAllListeners();
}

class BalanceLink{
    constructor(){
        this.link = 'http://' + location.hostname + ':2051';
        console.log(location);
        this.div = this.newDiv();
        this.text = this.newLink('Logs Balance');
        this.setButton(this.div, this.text, this.link);
    }
    setButton(container, content, link){
        container.content = content;
        container.link = link;
        container.setAttribute('onmousedown', 'window.location.replace(this.link,"_self");');
        container.setAttribute('onmouseover', 'this.style.backgroundColor = "#ffffff"; this.content.style.color = "black";');
        container.setAttribute('onmouseout', 'this.style.backgroundColor = "black"; this.content.style.color = "#ffffff";');
    }
    newDiv(){
        let div = document.body.appendChild(document.createElement('div'));
        div.style.position = "absolute";
        div.style.left = '0px';
        div.style.top = '0px';
        div.style.width = '256px';
        div.style.height = '64px';
        div.style.textAlign = "center";
        div.style.verticalAlign = "middle";
        div.style.backgroundColor = 'black';
        div.style.border = "1px solid #ffffff";
        div.style.zIndex = "2000";
        div.style.lineHeight = "30px";
        return div;
    }
    newLink(text){
        let link = this.div.appendChild(document.createElement('h1'));
        link.style.fontFamily = "Courier";
        link.style.verticalAlign = "middle";
        link.style.color = "#ffffff";
        link.innerHTML = text;
        return link;
    }
    destroy(){
        this.text.parentNode.removeChild(this.text);
        this.div.parentNode.removeChild(this.div);
    }
}

function gameOver(game){
    if(!tempInterface.drawn){
        const balance = new BalanceLink();
        ranking.reset();
        map = [];
        past.map = [];
        tiles.clear(true);
        playerList = {};
        for(let id in past.playerList){
            past.playerList[id].body.destroy();
            past.playerList[id].name.destroy();
            delete past.playerList[id];
        }
        past.playerList = {};
        for(let id in bombList){
            past.bombList[id].body.destroy();
            delete bombList[id];
        }
        bombList = {};
        game.scene.scene.cameras.main.x = 0;
        game.scene.scene.cameras.main.y = 0;
        tempInterface.graphics = game.add.graphics({
            lineStyle: { width: 2, color: 0xaa0000 },
            fillStyle: { color: 0x0000aa }
        });
        tempInterface.background = new Phaser.Geom.Rectangle();
        tempInterface.text = game.add.text(0, 0, client.murderer + '\nKilled you!\nTry Again..', {align: 'center'});
        tempInterface.drawn = true;
        game.input.on('pointerdown', function (pointer) {
            balance.destroy();
            restart(game);
        });
        game.input.keyboard.on('keydown', function (event){
            if(event.code == 'Enter'){
                balance.destroy();
                restart(game);
            }
        });
    }else{
        tempInterface.graphics.clear();
        tempInterface.background.width = Math.round(game.scene.scene.cameras.main.width/2);
        tempInterface.background.height = Math.round(game.scene.scene.cameras.main.height/2.5);
        tempInterface.background.centerX = Math.round(game.scene.scene.cameras.main.width/2);
        tempInterface.background.centerY = Math.round(game.scene.scene.cameras.main.height/2);
        tempInterface.text.setFontSize(Math.round((tempInterface.background.width/20) + (tempInterface.background.height/15)));
        tempInterface.text.x = Math.round(tempInterface.background.centerX - (tempInterface.text.width/2));
        tempInterface.text.y = Math.round(tempInterface.background.centerY - (tempInterface.background.height/3));
        tempInterface.graphics.fillRectShape(tempInterface.background);
        tempInterface.graphics.strokeRectShape(tempInterface.background);
    }
}

function updateMap(game){
    let newMap = [];
    tiles.clear(true);
    let tile;
    for(let i = 0; i < map.length; i++) {
        newMap.push([]);
        for(let j = 0; j < map.length; j++) {
            if(map[i][j] == 0)tile = game.add.image((i*TILESIZE)+(TILESIZE/2), (j*TILESIZE)+(TILESIZE/2), 'ground', Math.round(Math.random()*3));
            else{
                tile = game.add.image((i*TILESIZE)+(TILESIZE/2), (j*TILESIZE)+(TILESIZE/2), 'wall', 0);
            }
            tile.setDepth(1);
            tiles.add(tile);
            newMap[i][j] = map[i][j];
        }
    }
    return newMap;
};

class Abilities{
    constructor(player, abilities){
        this.player = player;
        this.mouse = {
            timeout: undefined,
            held: false,
            over : false
        }
        this.icons = [];
        this.icons.push(this.newIcon(this, abilities.movespeed.level, abilities.movespeed.cost, 'movespeed', 'client/assets/icon_movespeed.png', 'Increase Movement Speed<br>'));          //[0]movespeed
        this.icons.push(this.newIcon(this, abilities.cooldown.level, abilities.cooldown.cost, 'cooldown', 'client/assets/icon_cooldown.png', 'Decrease Bomb Cooldown<br>'));               //[1]cooldown
        this.icons.push(this.newIcon(this, abilities.bombRange.level, abilities.bombRange.cost, 'bombRange', 'client/assets/icon_range.png', 'Increase Bomb Range<br>'));            //[2]bombRange
        this.icons.push(this.newIcon(this, abilities.bombDelay.level, abilities.bombDelay.cost, 'bombDelay', 'client/assets/icon_delay.png', 'Increase Explosion Delay<br>'));           //[3]bombDelay
    }
    newText(text){
        let span = document.createElement('span');
        span.style.verticalAlign = "middle";
        span.style.color = "#ffffff";
        span.style.fontFamily = "Courier";
        span.innerHTML = text;
        return span;
    }
    newDescription(text, level, cost){
        let description = {};
        description.getStatus = function(level, cost){
            return "Level: " + level.toString() + ", Cost: " + cost.toString();
        };
        description.msg = this.newText(text);
        description.status = this.newText(description.getStatus(level, cost));
        description.div = document.createElement('div');
        document.body.appendChild(description.div);
        description.div.appendChild(description.msg);
        description.div.appendChild(description.status);
        let dWidth = function(){
            let w;
            if(description.status.offsetWidth > description.msg.offsetWidth){w = description.status.offsetWidth;}
            else{w = description.msg.offsetWidth;}
            w += 50;
            return w.toString();
        };
        let w = dWidth();
        description.div.style.width = w + 'px';
        description.div.style.position = "absolute";
        description.div.style.left = '64px';
        description.div.style.top = '0px';
        description.div.style.height = '62px';
        description.div.style.lineHeight = "30px";
        description.div.style.textAlign = "center";
        description.div.style.verticalAlign = "middle";
        description.div.style.backgroundColor = 'black';
        description.div.style.border = "1px solid #ffffff";
        description.div.style.zIndex = "2000";
        description.div.style.display = "none";
        return description;
    }
    newImage(abilities, name, src, description){
        let img = document.createElement('img');
        img.abilities = abilities;
        img.mouse = abilities.mouse;
        img.name = name;
        img.src = src;
        img.description = description;
        img.div = document.createElement('div');
        document.body.appendChild(img.div);
        img.div.appendChild(img);
        img.div.style.position = "absolute";
        img.div.style.left = '0px';
        img.div.style.top = ((this.icons.length) * 64).toString() + 'px';
        img.div.style.width = '64px';
        img.div.style.height = '64px';
        img.div.style.backgroundColor = 'black';
        img.div.style.zIndex = "2000";
        img.mouseManage = function(e){
            switch(e.type){
                case "mouseover":
                    this.mouse.over = true;
                    if(this.mouse.held){this.description.div.style.display = "inline-block";}
                break;
                case "mouseout":
                    this.description.div.style.display = "none";
                    this.mouse.over = false;
                break;
                case "mousedown":
                    if(!this.mouse.held){
                        clearTimeout(this.mouse.timeout);
                        let self = this;
                        this.mouse.timeout = setTimeout(function(){
                            self.mouse.held = true;
                            self.description.div.style.display = "inline-block";
                        }, 500, self);
                    }
                    else{
                        this.mouse.held = true;
                        this.description.div.style.display = "inline-block";
                    }
                break;
                case "mouseup":
                    if(!this.mouse.held && e.type == 'mouseup'){
                        this.abilities.upgrade(this.name);
                    }
                    clearTimeout(this.mouse.timeout);
                    this.description.div.style.display = "none";
                    img.mouse.held = false;
                break;
            }
        }
        img.setAttribute('draggable', 'false');
        img.setAttribute('onmouseover', 'this.mouseManage(event)');
        img.setAttribute('onmouseout', 'this.mouseManage(event)');
        img.setAttribute('onmousedown', 'this.mouseManage(event)');
        img.setAttribute('onmouseup', 'this.mouseManage(event)');
        return img;
    }
    newIcon(abilities, level, cost, name, src, description){
        let icon = {};
        icon.desc = this.newDescription(description, level, cost);
        icon.img = this.newImage(abilities, name, src, icon.desc);
        icon.level = level;
        icon.cost = cost;
        return icon;
    }
    upgrade(ability){
        return socket.emit('upgrade', ability);
    }
    update(){
        if(this.icons){
            for(let i in this.icons){
                if(this.player.exp >= this.icons[i].cost){
                    this.icons[i].img.div.style.display = 'inline-block';
                }
                else{this.icons[i].img.div.style.display = 'none';}
            }
        }
    }
    destroy(){
        for(let i in this.icons){
            let icon = this.icons[i];
            icon.img.parentNode.removeChild(icon.img);
            icon.img.div.parentNode.removeChild(icon.img.div);
            icon.desc.msg.parentNode.removeChild(icon.desc.msg);
            icon.desc.status.parentNode.removeChild(icon.desc.status);
            icon.desc.div.parentNode.removeChild(icon.desc.div);
        }
        delete this.icons;
    }
}

class Player{
    constructor(game, player){
        this.id = player.id;
        this.position = player.position;
        this.x = (this.position.x*TILESIZE)+(TILESIZE/2);
        this.y = (this.position.y*TILESIZE)+(TILESIZE/2);
        this.name = game.add.text(this.x, this.y, player.name);
        this.name.x -= (this.name.width/2);
        this.name.y -= TILESIZE;
        this.name.setDepth(4);
        this.alive = player.alive;
        this.cooling = player.cooling;
        this.coolingDelay = player.coolingDelay;
        this.moving = player.moving;
        this.movingDelay = player.movingDelay;
        if(this.id == client.id){
            client.player = this;
            this.abilities = new Abilities(this, player.abilities);
            this.body = game.add.sprite(this.x, this.y, 'playerClientStand', 0);
            this.body.anims.load('playerClientStandAnim');
            this.body.anims.load('playerClientSouthAnim');
            this.body.anims.load('playerClientNorthAnim');
            this.body.anims.load('playerClientEastAnim');
            this.body.anims.load('playerClientWestAnim');
            this.body.anims.play('playerClientStandAnim');
            showButtons(game);
        }else{
            this.body = game.add.sprite(this.x, this.y, 'playerEnemyStand', 0);
            this.body.anims.load('playerEnemyStandAnim');
            this.body.anims.load('playerEnemySouthAnim');
            this.body.anims.load('playerEnemyNorthAnim');
            this.body.anims.load('playerEnemyEastAnim');
            this.body.anims.load('playerEnemyWestAnim');
            this.body.anims.play('playerEnemyStandAnim');
        }
        this.body.setDepth(3);
        this.tween = {};
    }
    move(game, player){
        this.moving = player.moving;
        this.position = player.position;
        this.x = (this.position.x*TILESIZE)+(TILESIZE/2);
        this.y = (this.position.y*TILESIZE)+(TILESIZE/2);
        if(client.player.id == player.id){
            if(this.x > this.body.x){this.body.anims.play('playerClientEastAnim');}
            if(this.x < this.body.x){this.body.anims.play('playerClientWestAnim');}
            if(this.y > this.body.y){this.body.anims.play('playerClientSouthAnim');}
            if(this.y < this.body.y){this.body.anims.play('playerClientNorthAnim');}
        }else{
            if(this.x > this.body.x){this.body.anims.play('playerEnemyEastAnim');}
            if(this.x < this.body.x){this.body.anims.play('playerEnemyWestAnim');}
            if(this.y > this.body.y){this.body.anims.play('playerEnemySouthAnim');}
            if(this.y < this.body.y){this.body.anims.play('playerEnemyNorthAnim');}
        }
        if(this.moving){
            delete this.tween;
            let self = this;
            this.tween = game.tweens.add({
                targets: [self.body , self.name],
                x: function(target){
                    if(target.type == 'Sprite') return self.x;
                    else return self.x-(target.width/2);
                },
                y: function(target){
                    if(target.type == 'Sprite') return self.y;
                    else return self.y-TILESIZE;
                },
                ease: 'Power0',
                onComplete: function () { self.stand(self); },
                duration: self.movingDelay
            });
        }
        else{
            this.body.x = this.x;
            this.body.y = this.y;
            this.name.x = this.x;
            this.name.y = this.y;
        }
    }
    stand(self){
        if(self.id == client.id) self.body.anims.play('playerClientStandAnim');
        else self.body.anims.play('playerEnemyStandAnim');
    }
    upgrade(ability){
        let icon;
        switch(ability.name){
            case 'movespeed':
                icon = this.abilities.icons[0];
                this.movingDelay = ability.value;
                icon.cost = ability.cost;
                icon.level = ability.level;
            break;
            case 'cooldown':
                icon = this.abilities.icons[1];
                this.coolingDelay = ability.value;
                icon.cost = ability.cost;
                icon.level = ability.level;
            break;
            case 'bombRange':
                icon = this.abilities.icons[2];
                icon.cost = ability.cost;
                icon.level = ability.level;
            break;
            case 'bombDelay':
                icon = this.abilities.icons[3];
                icon.cost = ability.cost;
                icon.level = ability.level;
            break;
        }
        icon.desc.status.innerHTML = icon.desc.getStatus(icon.level, icon.cost);
    }
    die(game){
        if(this.id == client.id){
            this.abilities.destroy();
            stance = 'GAMEOVER';
        }
        this.body.destroy();
        this.name.destroy();
        delete playerList[this.id];
        delete past.playerList[this.id];
        if(stance == 'RUN'){
                let dead = game.add.sprite(this.x, this.y, 'die', 0);
                dead.setDepth(3);
                dead.anims.play('dieAnim');
                dead.on('animationcomplete', function(){dead.destroy()});
        }
    }
}

class Explosion{
    constructor(game, x, y){
        this.x = (x*TILESIZE)+(TILESIZE/2);
        this.y = (y*TILESIZE)+(TILESIZE/2);
        this.body = game.add.sprite(this.x, this.y, 'explo', 0);
        this.body.setDepth(4);
        this.body.anims.load('exploAnim');
        this.body.anims.play('exploAnim');
        let self = this.body;
        this.body.on('animationcomplete', function(){self.destroy()});
    }
}

class Bomb{
    constructor(game, bomb){
        this.id = bomb.id;
        this.playerId = bomb.playerId;
        this.alive = bomb.alive;
        this.delay = bomb.delay;
        this.range = bomb.range;
        this.position = bomb.position;
        this.x = (this.position.x*TILESIZE)+(TILESIZE/2);
        this.y = (this.position.y*TILESIZE)+(TILESIZE/2);
        this.body = game.add.sprite(this.x, this.y, 'shit', 0);
        this.body.setDepth(2);
        this.body.anims.load('shitAnim');
        this.body.anims.play('shitAnim');
    }
    explode(){
        this.body.destroy();
        delete bombList[this.id];
        delete past.bombList[this.id];
        delete this;
    }
}

class Ranking{
    constructor(game){
        this.text = game.add.text(0, 0, '');
        this.text.visible = false;
        this.text.setAlign('right');
        this.text.setDepth(10);
        this.data = {};
    }
    update(game){
        this.text.visible = true;
        this.text.x = game.scene.scene.cameras.main.x*-1 - this.text.width + (game.scene.scene.cameras.main.width)-25;
        this.text.y = game.scene.scene.cameras.main.y*-1 - this.text.height + (game.scene.scene.cameras.main.height)-25;
        let txt = '';
        if(this.data.length){
            for(let id in this.data){
                if(this.data[id].frag){txt += this.data[id].name + ' : ' + this.data[id].frag + '\n';}
            }
            this.text.setText(txt);
        }
    }
    reset(){
        this.data = {};
        this.text.visible = false;
        this.text.setText('');
    }
}

function checkChanges(game){
    if(past.map.length != map.length){
        past.map = updateMap(game);
    }
    for(let id in playerList){
        if(!past.playerList.hasOwnProperty(id)){
            past.playerList[id] = new Player(game, playerList[id]);
        }
        else if(past.playerList[id].position.x != playerList[id].position.x
            || past.playerList[id].position.y != playerList[id].position.y){
            past.playerList[id].move(game, playerList[id]);
        }
        if(playerList[id].alive == false){
            past.playerList[id].die(game);
        }
    }
    for(let id in bombList){
        if(!past.bombList.hasOwnProperty(id)){
            past.bombList[id] = new Bomb(game, bombList[id]);
        }
        if(bombList[id].alive == false){
            past.bombList[id].explode();
        }
    }
    if(client.player.alive){
        cameraFollow(game);
        client.player.abilities.update(game);
        ranking.update(game);
    }
    listenKeys(game);
};

function showButtons(game){
    let x = client.player.body.x;
    let y = client.player.body.y;
    let bomb = game.add.sprite(x,y,'buttons',0);
    let east = game.add.sprite(x+96,y,'buttons',1);
    let north = game.add.sprite(x,y-96,'buttons',2);
    let south = game.add.sprite(x,y+96,'buttons',3);
    let west = game.add.sprite(x-96,y,'buttons',4);
    bomb.setDepth(10);east.setDepth(10);north.setDepth(10);south.setDepth(10);west.setDepth(10);
    game.tweens.add({
        targets: [bomb,east,north,south,west],
        ease: 'Sine.easeInOut',
        duration: 1000,
        delay: 0,
        alpha: {
          getStart: () => 1,
          getEnd: () => 0
        },
        onComplete: () => {
          bomb.destroy();east.destroy();north.destroy();south.destroy();west.destroy();
        }
      });
}

function preload(){
    this.load.spritesheet('msIcon', 'client/assets/icon.png', { frameWidth: 64, frameHeight: 64 });
    this.load.spritesheet('buttons', 'client/assets/buttons.png', { frameWidth: 96, frameHeight: 96 });
    this.load.spritesheet('playerClientStand', 'client/assets/char/client_0.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('playerClientSouth', 'client/assets/char/client_1.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('playerClientNorth', 'client/assets/char/client_2.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('playerClientEast', 'client/assets/char/client_3.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('playerClientWest', 'client/assets/char/client_4.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('playerEnemyStand', 'client/assets/char/enemy_0.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('playerEnemySouth', 'client/assets/char/enemy_1.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('playerEnemyNorth', 'client/assets/char/enemy_2.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('playerEnemyEast', 'client/assets/char/enemy_3.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('playerEnemyWest', 'client/assets/char/enemy_4.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('die', 'client/assets/die.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('wall', 'client/assets/wall.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('ground', 'client/assets/ground.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('shit', 'client/assets/shit.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('explo', 'client/assets/explo.png', { frameWidth: 32, frameHeight: 32 });
};

function create(){
    resizeWindow();
    listenSocket(this);
    tiles = this.add.group();
    ////ANIMATIONS
    var shitAnim = this.anims.create({
        key: 'shitAnim',
        frames: this.anims.generateFrameNumbers('shit'),
        frameRate: 5,
        yoyo: true,
        repeat: -1
    });

    var exploAnim = this.anims.create({
        key: 'exploAnim',
        frames: this.anims.generateFrameNumbers('explo'),
        frameRate: 10,
    });
    var dieAnim = this.anims.create({
        key: 'dieAnim',
        frames: this.anims.generateFrameNumbers('die'),
        frameRate: 1,
        delay: 800
    });
    ////CLIENT CHARACTER ANIMATION
    var playerClientStandAnim = this.anims.create({
        key: 'playerClientStandAnim',
        frames: this.anims.generateFrameNumbers('playerClientStand'),
        frameRate: 4,
        yoyo: true,
        repeat: -1
    });

    var playerClientSouthAnim = this.anims.create({
        key: 'playerClientSouthAnim',
        frames: this.anims.generateFrameNumbers('playerClientSouth'),
        frameRate: 5,
        yoyo: true,
        repeat: -1
    });

    var playerClientNorthAnim = this.anims.create({
        key: 'playerClientNorthAnim',
        frames: this.anims.generateFrameNumbers('playerClientNorth'),
        frameRate: 5,
        yoyo: true,
        repeat: -1
    });

    var playerClientEastAnim = this.anims.create({
        key: 'playerClientEastAnim',
        frames: this.anims.generateFrameNumbers('playerClientEast'),
        frameRate: 5,
        yoyo: true,
        repeat: -1
    });

    var playerClientWest = this.anims.create({
        key: 'playerClientWestAnim',
        frames: this.anims.generateFrameNumbers('playerClientWest'),
        frameRate: 5,
        yoyo: true,
        repeat: -1
    });
    ////ENEMY CHARACTER ANIMATION
    var playerEnemyStandAnim = this.anims.create({
        key: 'playerEnemyStandAnim',
        frames: this.anims.generateFrameNumbers('playerEnemyStand'),
        frameRate: 4,
        yoyo: true,
        repeat: -1
    });

    var playerEnemySouthAnim = this.anims.create({
        key: 'playerEnemySouthAnim',
        frames: this.anims.generateFrameNumbers('playerEnemySouth'),
        frameRate: 5,
        yoyo: true,
        repeat: -1
    });

    var playerEnemyNorthAnim = this.anims.create({
        key: 'playerEnemyNorthAnim',
        frames: this.anims.generateFrameNumbers('playerEnemyNorth'),
        frameRate: 5,
        yoyo: true,
        repeat: -1
    });

    var playerEnemyEastAnim = this.anims.create({
        key: 'playerEnemyEastAnim',
        frames: this.anims.generateFrameNumbers('playerEnemyEast'),
        frameRate: 5,
        yoyo: true,
        repeat: -1
    });

    var playerEnemyWest = this.anims.create({
        key: 'playerEnemyWestAnim',
        frames: this.anims.generateFrameNumbers('playerEnemyWest'),
        frameRate: 5,
        yoyo: true,
        repeat: -1
    });

    ranking = new Ranking(this);
    
};

function update(){
    switch(stance){
        case 'INTRO':
        if(thisDevice.isMobile()){
            login(prompt('Type a name:'));
        }else{
            gameIntro(this);
        }
        break;
        case 'LOADING':
        break;
        case 'RUN':
        checkChanges(this);
        break;
        case 'GAMEOVER':
        gameOver(this);
        break;
    }
};