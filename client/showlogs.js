var socket = io();
socket.emit('showLogsDir');
class Interface{
    constructor(){
        this.tags = this.newPage();
    }
    removeChilds(tags){
        if(Array.isArray(tags)){
            for(let i in tags){
                while(tags[i].firstChild){
                    tags[i].removeChild(tags[i].firstChild);
                }
            }
        }else{
            while(tags.firstChild){
                tags.removeChild(tags.firstChild);
            }
        }
        while(document.body.firstChild){
            document.body.removeChild(document.body.firstChild);
        }
    }
    newPage(){
        if(Array.isArray(this.tags)){this.removeChilds(this.tags);}
        delete this.tags;
        this.tags = [];
        return this.tags;
    }
    isInPage(node){
        return (node === document.body) ? false : document.body.contains(node);
    }
    addTags(container, tags){
        if(Array.isArray(tags) && tags.length){
            for(let i in tags){
                container.appendChild(tags[i]);
            }
            this.tags.concat(tags);
        }else{
            container.appendChild(tags);
            this.tags.push(tags);
        }
        return tags;
    }
    newSpan(container, text){
        let tag = document.createElement('span');
        tag.innerHTML = text;
        tag.style.fontFamily = "Courier";
        return this.addTags(container, tag);
    }
    newLink(container, text, action, cb){
        let tag = document.createElement('h1');
        tag.innerHTML = text;
        tag.style.color = "black";
        tag.style.fontFamily = "Courier";
        tag.setAttribute(action, cb);
        tag.setAttribute('onmouseover', 'this.style.backgroundColor = "black";this.style.color = "white"');
        tag.setAttribute('onmouseout', 'this.style.backgroundColor = "white";this.style.color = "black"');
        tag.setAttribute(action, cb);
        return this.addTags(container, tag);
    }
    newDiv(container){
        let tag = document.createElement('div');
        tag.style.display = 'inline-block';
        tag.style.textAlign = "center";
        return this.addTags(container, tag);
    }
    showDir(paths){
        let tags = this.newPage();
        let container = this.newDiv(document.body);

        //CREATE LINK TO EACH DIR FILE
        if(paths.length){
            for(let i in paths){
                tags.push(this.newLink(container, paths[i], 'onmousedown', 'socket.emit("showLog", this.innerHTML)'));
            }
        }
        tags.push(container.appendChild(document.createElement('br')));
        tags.push(container.appendChild(document.createElement('br')));
        tags.push(this.newLink(container, 'Play the Game', 'onmousedown', 'window.location.replace("http://" + location.hostname + ":2050","_self");'));
    }
    newList(container, name, items){
        let tags = [];
        tags.push(container.appendChild(document.createElement('br')));
        tags.push(this.newSpan(container, name));
        let list = container.appendChild(document.createElement('ul'));
        tags.push(list);
        let item = {};
        for(let i in items){
            item = list.appendChild(document.createElement('li'));
            item.style.fontFamily = "Courier";
            item.innerHTML = items[i];
            tags.push(item);
        }
        tags.push(container.appendChild(document.createElement('br')));
        return this.addTags(container, tags);
    }
    newReport(name, container, obj){
        let tags = [];
        let store = {
            name : [],
            list : []
        };
        if(Array.isArray(obj)){
            tags.push(this.newList(container, name, obj));
        }else{
            if(typeof obj == 'object'){
                let list = [];
                let keys = Object.keys(obj);
                keys.forEach(function(key){
                    if(typeof obj[key] == 'object'){
                        store.name.push(key);
                        store.list.push(obj[key]);
                    }else{
                        list.push((key + ' = ' + obj[key]));
                    }
                }.bind(this));
                if(list.length){tags.push(this.newReport(name, container, list));}
            }else{
                tags.push(this.newSpan(container, (name + ' = ' + obj)));
                tags.push(container.appendChild(document.createElement('br')));
            }
        }
        if(store.list.length){
            for(let i in store.list){
                tags.push(this.newReport(store.name[i], container, store.list[i]));
            }
        }
        return tags;
    }
    showLog(log){
        let tags = this.newPage();
        let container = this.newDiv(document.body);

        //CREATE REPORT
        tags.push(this.newReport(log.date, container, log.report));

        //BACK TO DIR BUTTON
        tags.push(container.appendChild(document.createElement('br')));
        tags.push(container.appendChild(document.createElement('br')));
        tags.push(this.newLink(container, 'Back to directory', 'onmousedown', 'socket.emit("showLogsDir", this.innerHTML)'));
    }
}
const page = new Interface();


socket.on('showLog', function(data){
    page.showLog(data);
});
socket.on('showLogsDir', function(data){
    page.showDir(data);
});