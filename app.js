const express = require('express');
const app = express();
const serv = require('http').Server(app);

//START A SERVER BY EXPRESS
app.get('/', function(req, res){
    res.sendFile(__dirname + '/client/index.html');
});
app.use('/client', express.static(__dirname + '/client'));
serv.listen(2050, function() {
    console.log('Server ready on port %d', serv.address().port);
});

//SOCKETS MANAGEMENT
var io = require('socket.io')(serv,{});
var SOCKET_LIST = {};   //  index = socket.id , value = playerId
var playerId = 0;
var PLAYER_LIST = {};   //  index = playerId , value = player
var bombId = 0;
var BOMB_LIST = {};     //  index = bombId , value = bomb
var AI_LIST = {};       //  index = playerId , value = player

io.sockets.on('connection', function(socket){
    console.log('New client: ', playerId, socket.id);
    if(playerId > 999999999999){playerId = 0;}
    if(bombId > 999999999999){bombId = 0;}
    SOCKET_LIST[socket.id] = playerId;
    playerId++;

    socket.on('login', function(data){
        login(socket, data);
    });

    socket.on('key', function(data){
        let player = PLAYER_LIST[SOCKET_LIST[socket.id]];
        if(player && player.alive){
            if(data == 'space'){
                bombId = player.bomb(io, socket.id, BOMB_LIST, bombId, MAP, PLAYER_LIST, SOCKET_LIST);//findValue(SOCKET_LIST, self.id)
            }else{
                player.move(data, io, MAP);
            }
        }
    });

    socket.on('upgrade', function(data){
        if(PLAYER_LIST[SOCKET_LIST[socket.id]]){
            PLAYER_LIST[SOCKET_LIST[socket.id]].abilities.upgrade(PLAYER_LIST[SOCKET_LIST[socket.id]], data, io, socket.id);
        }
    });

    socket.on('disconnect', function(){
        if(PLAYER_LIST[SOCKET_LIST[socket.id]]){
            PLAYER_LIST[SOCKET_LIST[socket.id]].die('disconected', io, PLAYER_LIST);
        }
        delete SOCKET_LIST[socket.id];
        console.log('Client left: ', socket.id);
    });
});

const Map = require('./src/map');
const MAP = new Map(9);
const Player = require('./src/player');
const getRanking = require('./src/ranking');

//AI CLASS
class AI{
    constructor(id){
        this.id = id;
        this.body = new Player(id, 'BOT', MAP);
        PLAYER_LIST[this.id] = this.body;
        this.body.isAI = true;
        this.body.strategy = ['movespeed', 'cooldown', 'bombRange', 'bombDelay'];
        this.body.strategy = this.body.strategy[Math.floor(Math.random()*this.body.strategy.length)];
        io.sockets.emit('playerChanges', PLAYER_LIST[id]);
        this.manage();
    }
    manage(){
        if(this.body.alive){
            if(this.body.exp > 0){this.body.abilities.upgrade(this.body, this.body.strategy, io, findValue(SOCKET_LIST, this.body.id));}
            let movement = ["up", "down", "left", "right"];
            while(!this.body.move(movement[Math.floor(Math.random()*movement.length)], io, MAP)){}
            if(!this.body.cooling && Math.floor(Math.random()*4) == 0){bombId = this.body.bomb(io, false, BOMB_LIST, bombId, MAP, PLAYER_LIST, SOCKET_LIST);}//findValue(SOCKET_LIST, self.id)
            setTimeout(function(){
                this.manage(this);
            }.bind(this), 1000);
        }else{
            delete AI_LIST[this.id];
            delete this;
        }
    }
}
//AI CREATION
function createAI(){
    playerAmmount = Object.keys(PLAYER_LIST).length;
    if(playerAmmount < 20){
        AI_LIST[playerId] = new AI(playerId);
        playerId++;
        bombId = MAP.resize(io, PLAYER_LIST, BOMB_LIST, bombId, MAP, SOCKET_LIST);
    }
    setTimeout(function(){
        createAI();
    }, 3000);
}

const {findValue, findPropValue} = require('./src/util');

//LOGIN
function login(socket, data){
    if(data == '' || findPropValue(PLAYER_LIST, 'name', data)){
        socket.emit('login', false);
    }else{
        let player = new Player(SOCKET_LIST[socket.id], data, MAP);
        PLAYER_LIST[SOCKET_LIST[socket.id]] = player;
        socket.emit('clientId', player.id);
        socket.emit('playerList', PLAYER_LIST);
        socket.emit('bombList', BOMB_LIST);
        socket.emit('ranking', getRanking(PLAYER_LIST));
        socket.emit('login', true);
        socket.broadcast.emit('playerChanges', player);
        bombId = MAP.resize(io, PLAYER_LIST, BOMB_LIST, bombId, MAP, SOCKET_LIST);
    }
}

createAI();