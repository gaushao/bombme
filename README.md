# Bombme

This is a MMO Action Game based on Bomberman (Nintendo) mechanics. Thematic was slightly modified to be funnier and produce a moral background. Media robots fight eachother releasing explosive news in a high-tech arena in order to destroy themselves.

#### Features
- move and drop bombs with touchscreen support (touch own character to drop bombs)
- earn points killing others to grown in ranking
- spend earned points on abilities
  - movespeed increase
  - explosion range increase
  - explosion delay decrease
  - bomb cooldown decrease
- arena will grow and drop in size depending on the number of players
- AI players (they are all called BOT)
- report generator on a separate server creates json logs about game statistics

#### Install
> npm install

#### Run game server
> node app

#### Access game client
> http://localhost:2050/

#### Run report server
> node report

#### Access report client
> http://localhost:2051/
